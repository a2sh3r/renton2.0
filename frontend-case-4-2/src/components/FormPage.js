import React from "react";
import FormComponent from "./forms/FormComponent";

const FormPage = () => {
  return (
    <div>
      <FormComponent />
    </div>
  );
};

export default FormPage;
