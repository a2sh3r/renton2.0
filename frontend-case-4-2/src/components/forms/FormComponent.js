import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const FormComponent = () => {
  const [formData, setFormData] = useState({
    client_id: 1,
    age: 30,
    region: "Russia",
    gender: "Male",
    job: "Software Engineer",
    organization: "ABC Company",
    income: 50000,
    marital_status: "Married",
    ip_flag: true,
    sme_flag: false,
    employee_flag: true,
    refugee_flag: false,
    pdn: 75,
    credit_type: "Personal Loan",
    credit_purchase: "Home Renovation",
    product_code: "PRD123",
    term: 24,
    orig_amount: 10000,
    curr_rate_nval: 12,
    value_dt: "2024-05-26",
    overdue_ind: false,
    card_type: "Gold",
    card_product_code: "CRD456",
    cc_limit_nval: 2000,
    cc_grace_period: 30,
    cc_curr_rate: 0.1,
    cc_open_dt: "2023-01-01",
    cc_overdue_ind: false,
  });
  const [successMessage, setSuccessMessage] = useState("");

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    // Обрабатываем специальные случаи для даты и булевых значений
    const val = type === "checkbox" ? checked : value;
    setFormData({ ...formData, [name]: val });
  };
  const navigate = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://127.0.0.1:5000/save_form", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });
      const data = await response.json();
      setSuccessMessage(data.message);
      console.log("${formData.client_id}");
      navigate(`/credit/${formData.client_id}`);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="max-w-lg mx-auto mt-10 ">
      {successMessage && (
        <div
          className="bg-purple-100 border border-purple-400 text-purple-500 px-4 py-3 mb-4 rounded relative"
          role="alert"
        >
          <strong className="font-bold">Success!</strong>
          <span className="block sm:inline"> {successMessage}</span>
        </div>
      )}
      <div class="w-full shadow-2xl appearance-none border">
        <form
          onSubmit={handleSubmit}
          class="bg-white  rounded px-8 pt-6 pb-8 mb-4"
        >
          <div class="shadow mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Client ID:
            </label>
            <input
              type="text"
              name="client_id"
              value={formData.client_id}
              onChange={handleChange}
              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="inline-full-name"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Age:
            </label>
            <input
              type="number"
              name="age"
              value={formData.age}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>

          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Region:
            </label>
            <input
              type="text"
              name="region"
              value={formData.region}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Gender:
            </label>
            <input
              type="text"
              name="gender"
              value={formData.gender}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Job:
            </label>
            <input
              type="text"
              name="job"
              value={formData.job}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Organization:
            </label>
            <input
              type="text"
              name="organization"
              value={formData.organization}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Income:
            </label>
            <input
              type="number"
              name="income"
              value={formData.income}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Marital Status:
            </label>
            <input
              type="text"
              name="marital_status"
              value={formData.marital_status}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              IP Flag:
            </label>
            <input
              type="checkbox"
              name="ip_flag"
              checked={formData.ip_flag}
              onChange={handleChange}
              className="form-checkbox mt-4 block"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              SME Flag:
            </label>
            <input
              type="checkbox"
              name="sme_flag"
              checked={formData.sme_flag}
              onChange={handleChange}
              className="form-checkbox mt-4 block"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Employee Flag:
            </label>
            <input
              type="checkbox"
              name="employee_flag"
              checked={formData.employee_flag}
              onChange={handleChange}
              className="form-checkbox mt-4 block"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Refugee Flag:
            </label>
            <input
              type="checkbox"
              name="refugee_flag"
              checked={formData.refugee_flag}
              onChange={handleChange}
              className="form-checkbox mt-4 block"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              PDN:
            </label>
            <input
              type="number"
              name="pdn"
              value={formData.pdn}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Credit Type:
            </label>
            <input
              type="text"
              name="credit_type"
              value={formData.credit_type}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Credit Purchase:
            </label>
            <input
              type="text"
              name="credit_purchase"
              value={formData.credit_purchase}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Product Code:
            </label>
            <input
              type="text"
              name="product_code"
              value={formData.product_code}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Term:
            </label>
            <input
              type="number"
              name="term"
              value={formData.term}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Original Amount:
            </label>
            <input
              type="number"
              name="orig_amount"
              value={formData.orig_amount}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Current Rate Nval:
            </label>
            <input
              type="number"
              name="curr_rate_nval"
              value={formData.curr_rate_nval}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Value Date:
            </label>
            <input
              type="date"
              name="value_dt"
              value={formData.value_dt}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Overdue Indicator:
            </label>
            <input
              type="text"
              name="overdue_ind"
              value={formData.overdue_ind}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Card Type:
            </label>
            <input
              type="text"
              name="card_type"
              value={formData.card_type}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              Card Product Code:
            </label>
            <input
              type="text"
              name="card_product_code"
              value={formData.card_product_code}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              CC Limit Nval:
            </label>
            <input
              type="number"
              name="cc_limit_nval"
              value={formData.cc_limit_nval}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              CC Grace Period:
            </label>
            <input
              type="number"
              name="cc_grace_period"
              value={formData.cc_grace_period}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              CC Current Rate:
            </label>
            <input
              type="number"
              name="cc_curr_rate"
              value={formData.cc_curr_rate}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              CC Open Date:
            </label>
            <input
              type="date"
              name="cc_open_dt"
              value={formData.cc_open_dt}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <div class="mb-4">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-3 pr-4 flex items-center">
              CC Overdue Indicator:
            </label>
            <input
              type="text"
              name="cc_overdue_ind"
              value={formData.cc_overdue_ind}
              onChange={handleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            />
          </div>
          <button
            type="submit"
            className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded"
          >
            Submit Form
          </button>
        </form>
      </div>
    </div>
  );
};

export default FormComponent;
