import React from "react";
import { Link } from "react-router-dom";
import "./HomePage.css"; // Подключаем файл стилей

const HomePage = () => {
  return (
    <div className="flex justify-center items-center h-screen">
      <Link to="/form" className="big-button">
        Заполнить форму клиента
      </Link>
    </div>
  );
};

export default HomePage;
