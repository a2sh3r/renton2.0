import React from "react";
import renton from "../../assets/renton.svg";
const Footer = () => {
  return (
    <footer class="bg-white rounded-lg shadow light:bg-pink-900 m-4">
      <div class="w-full max-w-screen-xl mx-auto p-4 md:py-8">
        <div class="sm:flex sm:items-center sm:justify-between">
          <a
            href={"https://renhack.ru/#overpopup"}
            class="flex items-center m:mb-0 space-x-3 rtl:space-x-reverse"
          >
            <img src={renton} class="h-16" alt="Flowbite Logo" />
            <span class="self-center text-2xl font-regular whitespace-nowrap dark:text-pink">
              Ренесанс хакатон
            </span>
          </a>
          <ul class="flex flex-wrap items-center mb-6 text-sm font-medium text-pink-500 sm:mb-0 dark:text-pink-400">
            <li>
              <a
                href="https://gitlab.com/a2sh3r/renton2.0"
                class="hover:underline me-4 md:me-6"
              >
                Gitlab
              </a>
            </li>
          </ul>
        </div>
        <hr class="my-6 border-pink-200 sm:mx-auto dark:border-pink-700 lg:my-8" />
        <span class="block text-sm text-pink-500 sm:text-center dark:text-pink-400">
          © 2024{" "}
          <a href="#" class="hover:underline">
            KNIKAPUTTY™
          </a>
          . No rights :(
        </span>
      </div>
    </footer>
  );
};

export default Footer;
