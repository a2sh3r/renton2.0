import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";

const CheckCreditPage = () => {
  const { client_id } = useParams(); // Получение параметра client_id из URL
  const [formData, setFormData] = useState({}); // Состояние для хранения данных из формы
  const [creditMessage, setCreditMessage] = useState(""); // Сообщение о кредите
  const [loading, setLoading] = useState(true); // Состояние загрузки

  // Функция для вызова эндпоинта при загрузке страницы
  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true); // Начало загрузки
        const response = await fetch(
          `http://127.0.0.1:5000/check_credit/${client_id}`
        );
        const data = await response.json();
        setCreditMessage(data.message);
      } catch (error) {
        console.error("Error:", error);
      } finally {
        setLoading(false); // Конец загрузки
      }
    };

    fetchData();
  }, [client_id]); // Зависимость от client_id, чтобы вызывать эндпоинт при изменении ID

  // Обработчик изменения данных формы
  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    // Обрабатываем специальные случаи для даты и булевых значений
    const val = type === "checkbox" ? checked : value;
    setFormData({ ...formData, [name]: val });
  };

  // Обработчик отправки формы
  const handleSubmit = async (e) => {
    e.preventDefault();
    // Выполнение эндпоинта с данными из формы
    try {
      setLoading(true); // Начало загрузки
      const response = await fetch(
        `http://127.0.0.1:5000/check_credit/${client_id}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        }
      );
      const data = await response.json();
      setCreditMessage(data.message);
    } catch (error) {
      console.error("Error:", error);
    } finally {
      setLoading(false); // Конец загрузки
    }
  };

  return (
    <div className="min-h-screen flex justify-center items-center">
      <div className="max-w-lg mx-auto bg-rose-500 p-12 rounded-3xl bg-opacity-80">
        {loading ? (
          <div className="text-white text-center">Loading...</div>
        ) : (
          <>
            <h1 className="text-2xl font-light mb-4 text-white text-1xl">
              Check Credit Page
            </h1>
            <p className="mb-4 text-white font-bold text-2xl">
              Client ID: {client_id}
            </p>
            <p className="mb-4 text-white font-bold text-2xl">
              Credit Message: {creditMessage}
            </p>

            <Link
              to="/"
              className="bg-purple-500 hover:bg-purple-700 text-white font-bold py-6 px-24 mt-12 rounded-3xl block w-full text-center"
            >
              Go back to Home page
            </Link>
          </>
        )}
      </div>
    </div>
  );
};

export default CheckCreditPage;
