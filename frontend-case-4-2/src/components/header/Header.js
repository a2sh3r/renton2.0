import React from "react";
import { Link } from "react-router-dom";
import "./Header.css"; // Подключаем файл стилей

const Header = () => {
  return (
    <Link to="/" className="header">
      <img
        src={require("../../assets/header.png")}
        alt="Header Background"
        className="header-image"
      />
    </Link>
  );
};

export default Header;
