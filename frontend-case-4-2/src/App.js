import React from "react";
import logo from "./logo.svg";
import "./App.css";
import FormComponent from "./components/forms/FormComponent";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import FormPage from "./components/FormPage";
import HomePage from "./components/HomePage";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import CheckCreditPage from "./components/CheckCreditPage";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/form" element={<FormPage />} />
          <Route path="/credit/:client_id" element={<CheckCreditPage />} />
        </Routes>
      </div>
      <Footer />
    </Router>
  );
}

export default App;
