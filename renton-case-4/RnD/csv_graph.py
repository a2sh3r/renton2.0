import pandas as pd
import matplotlib.pyplot as plt

# Чтение данных из Excel файлов
client_ds = pd.read_excel('client_ds.xlsx')
credit_ds = pd.read_excel('credit_ds.xlsx')
card_ds = pd.read_excel('card_ds.xlsx')

# Объединение данных на основе CLIENT_ID
merged_df = pd.merge(client_ds, credit_ds, on='CLIENT_ID', how='left')
merged_df = pd.merge(merged_df, card_ds, on='CLIENT_ID', how='left')

# Удаление строк, где значение OVERDUE_IND равно null
filtered_df = merged_df.dropna(subset=['OVERDUE_IND'])

# Группировка данных по полу и наличию просрочек, подсчет количества клиентов
overdue_counts = filtered_df.groupby(['GENDER', 'OVERDUE_IND']).size().unstack()


