import pandas as pd
import matplotlib.pyplot as plt

# Чтение данных из Excel файлов
client_ds = pd.read_excel('client_ds.xlsx')
credit_ds = pd.read_excel('credit_ds.xlsx')
card_ds = pd.read_excel('card_ds.xlsx')

# Объединение данных на основе CLIENT_ID
merged_df = pd.merge(client_ds, credit_ds, on='CLIENT_ID', how='left')
merged_df = pd.merge(merged_df, card_ds, on='CLIENT_ID', how='left')

# Удаление строк, где значение OVERDUE_IND равно null
filtered_df = merged_df.dropna(subset=['OVERDUE_IND'])

# Группировка данных по полу и наличию просрочек, подсчет количества клиентов
overdue_counts = filtered_df.groupby(['GENDER', 'OVERDUE_IND']).size().unstack()

# Получение данных для гистограммы
male_overdue = filtered_df[filtered_df['GENDER'] == 1]['OVERDUE_IND']
female_overdue = filtered_df[filtered_df['GENDER'] == 2]['OVERDUE_IND']

# Построение гистограммы
plt.figure(figsize=(10, 6))
plt.hist([male_overdue, female_overdue], bins=2, color=['pink', 'blue'], alpha=0.7, label=['1 pol', '2 pol'])
plt.title('Гистограмма просрочек в зависимости от пола клиента')
plt.xlabel('Наличие просрочек (1 = Да, 0 = Нет)')
plt.ylabel('Количество клиентов')
plt.xticks([0.25, 0.75], ['Нет', 'Да'])
plt.legend()
plt.grid(axis='y')  # Отображение сетки только по оси y
plt.show()


plt.figure(figsize=(10, 6))
plt.scatter(filtered_df['INCOME'], filtered_df['OVERDUE_IND'], alpha=0.5)
plt.title('Отношение зарплаты к наличию просрочек')
plt.xlabel('Зарплата')
plt.ylabel('Наличие просрочек (1 = Да, 0 = Нет)')
plt.grid(True)
plt.show()


default_clients = merged_df[merged_df['OVERDUE_IND'] == 1]
payed_clients = merged_df[merged_df['OVERDUE_IND'] == 0]


def calculate_ratio(default, payed, column):
    default_count = default[column].value_counts()
    payed_count = payed[column].value_counts()
    ratio = default_count / payed_count
    return ratio.fillna(0)


marital_status_ratio = calculate_ratio(default_clients, payed_clients, 'MARITAL_STATUS')

# Построение гистограммы для семейного статуса
plt.figure(figsize=(10, 6))
marital_status_ratio.plot(kind='bar', color='skyblue', edgecolor='black', alpha=0.7)
plt.title('Отношение невыплаченных кредитов к выплаченным по семейному статусу')
plt.xlabel('Семейный статус')
plt.ylabel('Отношение невыплаченных к выплаченным')
plt.grid(axis='y')
plt.show()

