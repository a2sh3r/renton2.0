workspace "Renton Hackaton" "Диаграмма компонентов для кейса 4.2" {

    model {
        user = person "Пользователь" "Пользователь банка, обратившийся за помощью к сотруднику"
        bank_employee = person "Сотрудник банка" "Сотрудник банка к которому обратился клиент для выяснения возможности получения кредита."

        cc = softwareSystem "CritCredit" "Deploying vcluster service." {
            webApp = container "Веб сервер" "Веб сервер для сотрудника банка." "React"
            backend = container "Бекенд сервер" "Бекенд сервер для обработки запросов с фронтенда" "Flask"
        }

        ai = softwareSystem "Модель нейронной сети" "Натренированная модель нейронной сети" "AI"
        postgres = softwareSystem "PostgreSQL" "База данных клиентов"

        user -> bank_employee "Обращается"
        bank_employee -> webApp "Использует"
        webApp -> backend "API"
        backend -> ai "JSON"
        ai -> backend "JSON"
        backend -> postgres "SQL Alchemy"
    }

    views {
        container cc "CritCredit" {
            include *
            autolayout lr
        }

        theme default
    }

}
