import pandas as pd
from sklearn.preprocessing import OneHotEncoder

def onehot(df, label):
    # Create a copy of the DataFrame
    df_onehot = df.copy()

    # Apply OneHotEncoding to the selected column
    onehotencoder = OneHotEncoder(sparse_output=False)
    encoded_features = onehotencoder.fit_transform(df_onehot[[label]])
    encoded_df = pd.DataFrame(encoded_features, columns=onehotencoder.get_feature_names_out())

    # Concatenate encoded features with the original DataFrame
    df_onehot = pd.concat([df_onehot, encoded_df], axis=1)

    # Drop the original label column
    df_onehot = df_onehot.drop(label, axis=1)

    return df_onehot

def par_plus_conc():
    # Чтение данных из Excel файлов
    client_ds = pd.read_excel('dataset/client_ds.xlsx')
    credit_ds = pd.read_excel('dataset/credit_ds.xlsx')
    client_ds = onehot(client_ds, 'MARITAL_STATUS')
    credit_ds = onehot(credit_ds, 'PRODUCT_CODE')
    card_ds = pd.read_excel('dataset/card_ds.xlsx')

    # Объединение данных на основе CLIENT_ID
    merged_df = pd.merge(client_ds, credit_ds, on='CLIENT_ID', how='left')
    #merged_df = pd.merge(merged_df, card_ds, on='CLIENT_ID', how='left')

    return merged_df


def save_df(df):
    df.to_excel('dataset/merged_data.xlsx')


def data_clear(df):
    filtered_df = df[~df['OVERDUE_IND'].isna()]
    del filtered_df['REGION']
    del filtered_df['JOB']
    del filtered_df['ORGANIZATION']
    del filtered_df['IP_FLAG']
    del filtered_df['SME_FLAG']
    del filtered_df['EMPLOYEE_FLAG']
    del filtered_df['REFUGEE_FLAG']
    del filtered_df['CREDIT_TYPE']
    del filtered_df['CREDIT_PURCHASE']

    return filtered_df


def formate(df):
    # Преобразуем 'VALUE_DT' в секунды с начала 1970 года
    df['VALUE_DT_SECONDS'] = (df['VALUE_DT'] - pd.Timestamp('1970-01-01')) // pd.Timedelta('1s')
    del df['VALUE_DT']
    # Преобразование TERM в числовое значение
    df['TERM'] = df['TERM'].str.replace('M', '').astype(int)

    return df


if __name__ == '__main__':
    data = par_plus_conc()
    curr_df = data_clear(data)
    curr_df = formate(curr_df)
    save_df(curr_df)