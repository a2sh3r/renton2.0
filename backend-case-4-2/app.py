from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_migrate import Migrate
from flask_admin.contrib.sqla import ModelView
from flask_cors import CORS
from func_server import predict_overdue_probability 

app = Flask(__name__)
CORS(app)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:123rty@localhost/postgres'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class ClientForm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer())
    age = db.Column(db.Integer)
    region = db.Column(db.String(50))
    gender = db.Column(db.String(10))
    job = db.Column(db.String(100))
    organization = db.Column(db.String(100))
    income = db.Column(db.Float)
    marital_status = db.Column(db.String(20))
    ip_flag = db.Column(db.Boolean)
    sme_flag = db.Column(db.Boolean)
    employee_flag = db.Column(db.Boolean)
    refugee_flag = db.Column(db.Boolean)
    pdn = db.Column(db.Float)
    credit_type = db.Column(db.String(50))
    credit_purchase = db.Column(db.String(100))
    product_code = db.Column(db.String(50))
    term = db.Column(db.Integer)
    orig_amount = db.Column(db.Float)
    curr_rate_nval = db.Column(db.Float)
    value_dt = db.Column(db.String(20))
    overdue_ind = db.Column(db.Boolean)
    card_type = db.Column(db.String(50))
    card_product_code = db.Column(db.String(50))
    cc_limit_nval = db.Column(db.Float)
    cc_grace_period = db.Column(db.Integer)
    cc_curr_rate = db.Column(db.Float)
    cc_open_dt = db.Column(db.String(20))
    cc_overdue_ind = db.Column(db.Boolean)
    approved_credit = db.Column(db.Boolean, default=True)
    overdue_probability = db.Column(db.Float, default=1)


@app.route('/save_form', methods=['POST'])
def save_form():
    data = request.json
    print(data)
    gender = 1 if data['gender']=='Male' else 2
    print(type(gender), type(data['income']), type(data['pdn']), type(data['term']), type(data['orig_amount']))
    data['overdue_probability'] = float(predict_overdue_probability(data['age'], gender, int(data['income']), float(data['pdn']), int(data['term']), float(data['orig_amount']), float(data['curr_rate_nval'])))
    data['approved_credit'] = True if data['overdue_probability'] < 0.5 else False
    new_client = ClientForm(**data)
    db.session.add(new_client)
    db.session.commit()
    return jsonify({'message': 'Form saved successfully'}), 201


@app.route('/check_credit/<int:client_id>', methods=['GET'])
def check_credit(client_id):
    client = ClientForm.query.filter_by(client_id=client_id).first()
    if not client:
        return jsonify({'message': 'Client not found'}), 404
    if client.approved_credit:
        return jsonify({'message': 'Client is eligible for credit'}), 200
    else:
        return jsonify({'message': 'Client is not eligible for credit'}), 200


admin = Admin(app, name='Admin Panel', template_mode='bootstrap3')
admin.add_view(ModelView(ClientForm, db.session))

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True)
