import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

import config

if __name__ == '__main__':
    # Загрузка данных
    df = pd.read_excel("dataset/merged_data.xlsx")

    # Фильтрация данных
    df = df[~df['OVERDUE_IND'].isna()]

    list_param_uniq = ['AGE', 'GENDER', 'INCOME', 'PDN', 'TERM', 'ORIG_AMOUNT', 'CURR_RATE_NVAL',]
    # Выбор признаков
    X = df[list_param_uniq]
    y = df['OVERDUE_IND']

    # Нормализация данных
    scaler = StandardScaler()
    X = scaler.fit_transform(X)

    # Создание объекта PCA
    pca = PCA(n_components=7)  # Установите число компонент
    pca.fit(X)

    # Вывод долей объясненной дисперсии для каждой компоненты
    plt.figure(figsize=(10, 8))
    plt.bar(range(1, len(pca.explained_variance_ratio_) + 1), pca.explained_variance_ratio_)
    plt.xlabel('Параметр', fontsize=18)
    plt.ylabel('Доля влияния', fontsize=18)
    plt.title('Анализ влияния параметров', fontsize=20)

    list_param_uniq_rus = ['Возраст', 'Пол', 'Доход', 'ПДН', 'Срок кредита', 'Сумма кредита', 'Процентная ставка']
    plt.xticks(range(1, len(pca.explained_variance_ratio_) + 1), list_param_uniq_rus, fontsize=12, rotation=20,)

    plt.show()