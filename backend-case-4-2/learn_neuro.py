import pandas as pd
import tensorflow as tf
import torch
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import config


if __name__ == "__main__":
    df = pd.read_excel("dataset/merged_data.xlsx")
    # Разделение данных на обучающую, валидационную и тестовую выборки
    X = df[config.list_param]
    y = df['OVERDUE_IND']
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )
    X_train, X_val, y_train, y_val = train_test_split(
        X_train, y_train, test_size=0.25, random_state=42
    )

    # Масштабирование признаков
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.transform(X_val)
    X_test = scaler.transform(X_test)

    model = tf.keras.models.Sequential(
        [
            tf.keras.layers.Dense(1024, activation="tanh", input_shape=(X_train.shape[1],)),  # tanh
            tf.keras.layers.Dense(256, activation="relu"),
            tf.keras.layers.Dense(64, activation="relu"),  # Новый слой
            tf.keras.layers.Dense(1, activation="sigmoid"),
        ]
    )

    # Компиляция модели
    model.compile(
        optimizer="adam",
        loss="binary_crossentropy",
        metrics=["accuracy"],
    )

    # Обучение модели
    model.fit(X_train, y_train, epochs=100, batch_size=64, validation_data=(X_val, y_val))

    # Оценка модели
    loss, accuracy = model.evaluate(X_test, y_test, verbose=0)
    print("Test Loss:", loss)
    print("Test Accuracy:", accuracy)

    model.save("model/my_model.h5")
