import tensorflow as tf
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import config


def predict_overdue_probability(age, gender, income, pdn_start, term, orig_amount, curr_rate_nval):
    """
    Предсказывает вероятность просрочки платежа по кредиту.

    Args:
        age: Возраст заемщика.
        gender: Пол заемщика (1 - мужской, 0 - женский).
        income: Доход заемщика.
        pdn_start: Начальное значение платежа по долгу.
        term: Срок кредита.
        orig_amount: Сумма кредита.
        curr_rate_nval: Текущая процентная ставка.

    Returns:
        Вероятность просрочки платежа.
    """

    model = tf.keras.models.load_model('model/my_model.h5')

    pdn = 100 * (pdn_start/100*income + orig_amount)/income

    # Создайте DataFrame с данными одного человека
    new_data = pd.DataFrame({
        "AGE": [age],
        "GENDER": [gender],
        "INCOME": [income],
        "PDN": [pdn],
        "TERM": [term],
        "ORIG_AMOUNT": [orig_amount],
        "CURR_RATE_NVAL": [curr_rate_nval],
    })

    # Масштабирование признаков
    scaler = StandardScaler()
    df = pd.read_excel("dataset/merged_data.xlsx")
    # Разделение данных на обучающую, валидационную и тестовую выборки
    X = df[config.list_param]
    y = df['OVERDUE_IND']
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )
    X_train, X_val, y_train, y_val = train_test_split(
        X_train, y_train, test_size=0.25, random_state=42
    )

    scaler.fit(X_train)  # Используйте scaler, обученный на обучающей выборке
    new_data_scaled = scaler.transform(new_data)

    # Предсказание модели
    prediction = model.predict(new_data_scaled)

    # Преобразование прогноза в вероятность
    probability = prediction[0][0]

    return probability


if __name__=="__main__":
    # Пример использования функции
    age = 38
    gender = 2
    income = 61000
    pdn_start = 17.1
    term = 24
    orig_amount = 19000
    curr_rate_nval = 14.5

    probability = predict_overdue_probability(age, gender, income, pdn_start, term, orig_amount, curr_rate_nval)

    # Вывод результата
    if probability >= 0.5:
        print("Вероятность просрочки: {:.2f}% - Высокая".format(probability * 100))
    else:
        print("Вероятность просрочки: {:.2f}% - Низкая".format(probability * 100))